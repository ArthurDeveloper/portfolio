import Image from 'next/image';
import htmlLogo from '../../public/html-5.png';
import cssLogo from '../../public/css-3.png';
import jsLogo from '../../public/logo-js.webp';
import reactLogo from '../../public/react.png';
import nextLogo from '../../public/nextjs-logo.svg';
import nodeLogo from '../../public/nodejs.png';
import sassLogo from '../../public/sass.png';
import pythonLogo from '../../public/py.png'
import styles from '../styles/Skills.module.scss';

export default function Skills() {
    return (
        <div id={styles['skills-container']}>
            <div className={styles['skill']}>
                <span style={{color: '#ff4603'}}>
                    HTML
                </span>
                
                <div>
                    <Image src={htmlLogo} alt="HTML logo" width="130px" height="130px" />
                </div>
            </div>

            <div className={styles['skill']}>
                <span style={{color: '#34b4eb'}}>
                    CSS
                </span>

                <div>
                    <Image src={cssLogo} alt="CSS logo" width="130px" height="130px"/>
                </div>
            </div>

            <div className={styles['skill']}>
                <span style={{color: '#FFFF00'}}>
                    JS
                </span>

                <div>
                    <Image src={jsLogo} alt="JS logo" width="130px" height="130px" />
                </div>
            </div>

            <div className={styles['skill']}>
                <span style={{color: '#00FFFF'}}>
                    REACT
                </span>

                <div>
                    <Image src={reactLogo} alt="REACT logo" width="130px" height="130px"/>
                </div>
            </div>

            <div className={styles['skill']}>
                <span style={{color: 'var(--secondary-color)'}}>
                    NEXT
                </span>

                <div>
                    <Image src={nextLogo} alt="NEXT logo" width="130px" height="130px"/>
                </div>
            </div>

            <div className={styles['skill']}>
                <span style={{color: '#00FF00'}}>
                    NODE JS
                </span>

                <div>
                    <Image src={nodeLogo} alt="NODE logo" width="210px" height="130px"/>
                </div>
            </div>

            <div className={styles['skill']}>
                <span style={{color: '#FF88FF'}}>
                    SASS
                </span>

                <div>
                    <Image src={sassLogo} alt="SASS logo" width="200px" height="130px"/>
                </div>
            </div>

            <div className={styles['skill']}>
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <span style={{color: '#0055FF'}}>PY</span>
                    <span style={{color: '#FFFF00'}}>THON</span>
                </div>

                <div>
                    <Image src={pythonLogo} alt="PYTHON logo" width="140px" height="136px"/>
                </div>
            </div>
        </div>
    );
}