import { useState, useEffect } from 'react';

export default function TypeWriter({ children }) {
    const [text, setText] = useState(children);
    const [typedText, setTypedText] = useState('');
    let charIndex = 0;

    useEffect(() => {
        const intervalID = setInterval(() => {
            if (charIndex > text.length) {
                clearInterval(intervalID);
            }
            setTypedText(text.substring(0, charIndex));
            charIndex++;
        }, 50);
    }, []);

    return (
        <>
            { typedText }
        </>
    )
}