import Image from 'next/image';
import certificateAlgorithm from '../../public/certificado-algoritmo.png';
import certificatePython1 from '../../public/certificado-python-mundo1.png';
import certificatePython2 from '../../public/certificado-python-mundo2.png';
import styles from '../styles/Certificates.module.scss';

export default function Certificates() {
    return (
        <div id={styles['certificates-container']}>
            <h2 id={styles['certificates']}>
                Certificados
            </h2>

            <div className={styles['certificate-container']}>
                <div className={styles['certificate']}>
                    <div className={styles['certificate-image']}>
                        <Image src={certificateAlgorithm} width="274px" height="215px"/>
                    </div>
                    <div className={styles['certificate-data']}>
                        <span className={styles['course-name']}>ALGORITMO</span>
                        <span className={styles['course-workload']}>40 horas</span>
                        <span className={styles['course-conclusion-date']}>27/09/20</span>
                        <span className={styles['certificate-code']}>939B-6297-7</span>
                    </div>
                </div>
            </div>

            <div className={styles['certificate-container']}>
                <div className={styles['certificate']}>
                    <div className={styles['certificate-image']}>
                        <Image src={certificatePython1} width="274px" height="215px"/>
                    </div>
                    <div className={styles['certificate-data']}>
                        <span className={styles['course-name']}>PYTHON 3 - MUNDO 1</span>
                        <span className={styles['course-workload']}>40 horas</span>
                        <span className={styles['course-conclusion-date']}>29/07/20</span>
                        <span className={styles['certificate-code']}>939B-66E2-4</span>
                    </div>
                </div>
            </div>

            <div className={styles['certificate-container']}>
                <div className={styles['certificate']}>
                    <div className={styles['certificate-image']}>
                        <Image src={certificatePython2} width="274px" height="215px"/>
                    </div>
                    <div className={styles['certificate-data']}>
                        <span className={styles['course-name']}>PYTHON 3 - MUNDO 2</span>
                        <span className={styles['course-workload']}>40 horas</span>
                        <span className={styles['course-conclusion-date']}>28/09/20</span>
                        <span className={styles['certificate-code']}>939B-67AC-7</span>
                    </div>
                </div>
            </div>
        </div>
    )
}