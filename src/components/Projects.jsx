import Image from 'next/image';
import ToDoList from '../../public/todo-list.png';
import pomodoroClock from '../../public/relogio-pomodoro.png';
import richTextEditor from '../../public/rich-text-editor.png';
import theNEWSletter from '../../public/the-newsletter.png';
import styles from '../styles/Projects.module.scss';

export default function Projects() {
    return (
        <div id={styles['projects-container']}>
            <h2 id={styles['projects']}>
                Projetos realizados
            </h2>

            <div className={styles['project']}>
                <Image src={ToDoList} />
                <h3 className={styles['project-title']}>
                    Lista ToDo
                </h3>
                <p className={styles['project-description']}>
                    Uma aplicação web que faz com que seja possível criar uma lista de tarefas
                    para serem feitas, serve para organização diária.
                </p>
                <div className={styles['project-info']}>
                    <h4>Funcionalidades:</h4> 
                    <ul>
                        <li>Listar tarefas</li>
                        <li>Deletar tarefas</li>
                        <li>Marcar tarefas como concluídas</li>
                    </ul>

                    <h4>Tecnologias usadas:</h4> 
                    <ul>
                        <li>React</li>
                    </ul>
                </div>
                
                <a href="https://arthurdeveloper.github.io/todo-list"
                   target="_blank"
                   className={styles['project-link']}>
                    Acessar
                </a>                
            </div>

            <div className={styles['project']}>
                <Image src={pomodoroClock} />
                <h3 className={styles['project-title']}>
                    Relógio pomodoro
                </h3>
                <p className={styles['project-description']}>
                    Uma aplicação web que ajuda o usuário a praticar o ciclo pomodoro,
                    defininido tempo de trabalho e intervalos
                </p>
                <div className={styles['project-info']}>
                    <h4>Funcionalidades:</h4> 
                    <ul>
                        <li>Iniciar temporizador</li>
                        <li>Parar temporizador</li>
                        <li>Definir tempo de ciclo e tempo de descanço</li>
                    </ul>

                    <h4>Diferenciais:</h4> 
                    <ul>
                        <li>Temporizador altamente personalizável, é possível mudar
                            o tempo de ciclo, o tempo de descanço e o tempo de descanço após
                            o último ciclo.
                        </li>
                    </ul>
                    
                    <h4>Tecnologias usadas:</h4> 
                    <ul>
                        <li>React</li>
                    </ul>
                </div>
                <a href="https://arthurdeveloper.github.io/relogio-pomodoro"
                   target="_blank"
                   className={styles['project-link']}>
                    Acessar
                </a>                
            </div>

            <div className={styles['project']}>
                <Image src={richTextEditor} />
                <h3 className={styles['project-title']}>
                    Editor de textos rich text
                </h3>
                <p className={styles['project-description']}>
                    Uma aplicação que faz com que seja possível escrever textos e 
                    formatá-los, colocando-os como negrito, itálico, sublinhado, etc.
                </p>
                <div className={styles['project-info']}>
                    <h4>Funcionalidades:</h4> 
                    <ul>
                        <li>Escrever textos</li>
                        <li>Escrever cabeçalho para o texto</li>
                        <li>Formatar o texto</li>
                    </ul>

                    <h4>Diferenciais:</h4> 
                    <ul>
                        <li>
                            Formatação que permite o uso de negrito, itálico, sublinhado
                            e formatação de listas no seu texto.
                        </li>

                        <li>
                            Possibilidade de separar header (cabeçalho) de corpo
                        </li>
                    </ul>
                    
                    <h4>Tecnologias usadas:</h4> 
                    <ul>
                        <li>HTML</li>
                        <li>CSS</li>
                        <li>JS</li>
                    </ul>
                </div>
                <a href="https://arthurdeveloper.github.io/rich-text-editor"
                   target="_blank"
                   className={styles['project-link']}>
                    Acessar
                </a>          
            </div>

            <div className={styles['project']}>
                <Image src={theNEWSletter} />
                <h3 className={styles['project-title']}>
                    The NEWSletter
                </h3>
                <p className={styles['project-description']}>
                    Uma aplicação que permite você postar novas notícias e olhar as notícias
                    publicadas por outras pessoas.
                </p>
                <div className={styles['project-info']}>
                    <h4>Funcionalidades:</h4> 
                    <ul>
                        <li>Escrever notícias</li>
                        <li>Ver as notícias cadastradas</li>
                    </ul>

                    <h4>Diferenciais:</h4> 
                    <ul>
                        <li>
                            Possibilidade de postar suas notícias
                        </li>
                    </ul>
                    
                    <h4>Tecnologias usadas:</h4> 
                    <ul>
                        <li>Next</li>
                        <li>Node</li>
                        <li>Firebase</li>
                    </ul>
                </div>
                <a href="https://the-news-letter.herokuapp.com/"
                   target="_blank"
                   className={styles['project-link']}>
                    Acessar
                </a>          
            </div>
        </div>
    )
} 