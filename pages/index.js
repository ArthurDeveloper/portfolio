import Head from 'next/head';
import Image from 'next/image';
import TypeWriter from '../src/components/TypeWriter';
import Skills from '../src/components/Skills';
import Certificates from '../src/components/Certificates';
import Projects from '../src/components/Projects';
import Logos from '../src/components/Logos';
import styles from '../src/styles/Home.module.scss';

export default function Home() {
  return (
    <div>
      <Head>
        <meta name="author" content="Arthur Dev" />
        <meta name="keywords" content="Arthur, Dev, Developer, portfólio, portfolio, projetos, skills" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Arthur Dev</title>
      </Head>

      <div id="content">
        <header id={styles['page-header']}>
          <h1 className="underline-effect" id={styles['author-name']}>
            <TypeWriter>
              Arthur Dev
            </TypeWriter>
          </h1>
        </header>

        <div id={styles['presentation']}>
          <div id={styles['presentation-text']}>
            <h1 id={styles['presentation-title']}>
              <TypeWriter>
                Olá, eu sou o Arthur
              </TypeWriter>
            </h1>
            <h2 id={styles['presentation-subtitle']}>
              Programador e gamer nas horas vagas. <br/>
              Desenvolvo aplicações web e estudo desenvolvimento de games.<br/>
              <a href="https://github.com/arthurdeveloper" target="_blank">Github</a>
            </h2>
          </div>
        </div>

        <h1 style={{color: '$secondary-color', textAlign: 'center'}}>
          <TypeWriter>
            Habilidades
          </TypeWriter>
        </h1>

        <Skills />
        <Certificates />
        <Projects />
        <Logos />
      </div>

      <footer>
        <div>
            Icons made by <a href="https://www.freepik.com" title="Freepik">
                Freepik
            </a> from <a href="https://www.flaticon.com/" title="Flaticon">
                www.flaticon.com
            </a>
        </div>
      </footer>

    </div>
  );
}
